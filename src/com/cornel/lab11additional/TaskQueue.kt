package com.cornel.lab11additional

import java.util.*
import kotlin.concurrent.thread

class TaskQueue(private val threadCount: Int = 4) : Thread() {
    @Volatile private var queue = mutableListOf<MyTask>()
    @Synchronized private fun size() = queue.size
    private val threads = mutableListOf<Thread>()

    override fun start() {
        for(i in 1..threadCount) {
            val t = thread(start = true) {
                println("Thread $i: Hello!")
                while(true) {
                    try {
                        val task = pop()

                        for (number in task.from..task.to) {
                            val maxDivider = Math.floor(Math.sqrt(number.toDouble())).toInt()
                            for (divider in 2..maxDivider) {
                                if (number % divider == 0) break
                                if (divider == maxDivider) task.answers.add(number)
                            }
                        }

                        task.solved = true
                        push(task)
                    } catch (e: EmptyStackException) {
                        break
                    }
                }
                println("Thread $i: Bye!")
            }
            threads.add(t)
        }

        for (t in threads) {
            t.join()
        }
    }

    @Synchronized private fun pop(): MyTask {
        if (size() != 0 && !queue[0].solved) {
            val task = queue[0]
            queue.removeAt(0)
            return task
        } else {
            throw EmptyStackException()
        }
    }

    @Synchronized private fun push(task: MyTask) {
        queue.add(task)
    }

    fun push(from: Int, to: Int) {
        push(MyTask(from, to))
    }

    fun showResult() {
        for(elem in queue) {
            println(elem.answers.joinToString(", "))
        }
    }
}
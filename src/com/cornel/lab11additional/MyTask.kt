package com.cornel.lab11additional

class MyTask(from: Int, to: Int) {
    val from: Int
    val to: Int
    val answers = mutableListOf<Int>()
    var solved: Boolean = false
        set(value) {
            if (value) field = true // Can not be unsolved
        }
    init {
        if (from > to) {
            this.from = to
            this.to = from
        } else {
            this.from = from
            this.to = to
        }
    }
}
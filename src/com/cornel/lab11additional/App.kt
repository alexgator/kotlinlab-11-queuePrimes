package com.cornel.lab11additional

import java.lang.Thread.sleep

/*
 * Реализовать многопоточную программу, которая находит простые
 * числа в заданных интервалах. Есть класс TaskQueue, реализованный на основе
 * LinkedList с методами:
 * - MyTask pop() - взять очередное задание
 * - void push(MyTask) - положить очередное задание
 * - void showResult() - вывод результатов на экран
 * Класс MyTask +
 * - int start +
 * - int end +
 * ArrayList<Int> answers +
 *
 * Перед началом работы очередь наполняется заданиями. После этого потоки
 * забирают задачи и складывают результаты в ту же очередь.
 */

fun main(args: Array<String>) {
    val t = TaskQueue(4)
    t.push(0, 1000)
    t.push(20000, 30000)
    t.push(40000, 50000)
    t.push(100000, 90000)
    t.push(110000, 120000)
    t.start()
    t.join()
    t.showResult()
}
